from packet_data_obj.packet_data import PacketData


class PcapFileData:

    def __init__(self, nr_of_packets: int, file_name: str, packet_size: int, nr_of_channels: int, nr_of_stations):
        self.__packets = []
        self.__nr_of_packets = nr_of_packets
        self.__file_name = file_name
        self.__packet_size = packet_size
        self.__nr_of_channels = nr_of_channels
        self.__nr_of_stations = nr_of_stations
        self.channelid_meta_data = None
        self.channelfreq_meta_data = None
        self.timesamples_list = None
        self.channelized_data = None
        self.unix_epoch_time_delta = None

    @property
    def file_name(self):
        return self.__file_name

    @property
    def nr_of_packets(self):
        return self.__nr_of_packets

    @property
    def packet_size(self):
        return self.__packet_size

    @property
    def nr_of_channels(self):
        return self.__nr_of_channels

    @property
    def packets(self):
        return self.__packets

    @property
    def nr_of_stations(self):
        return self.__nr_of_stations

    def add_packet(self, packet: PacketData):
        self.__packets.append(packet)

    def set_channelid_meta_data(self, value):
        self.channelid_meta_data = value
        return self

    def set_channelfreq_meta_data(self, value):
        self.channelfreq_meta_data = value
        return self

    def set_timesamples_list(self, value):
        self.timesamples_list = value
        return self

    def set_channelized_data(self, value):
        self.channelized_data = value
        return self
