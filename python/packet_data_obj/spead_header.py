class SpeadHeader:
    def __init__(self, logical_channel_id,
                 unix_epoch_time,
                 center_freq,
                 frequency,
                 csp_channel_info,
                 beam_id,
                 frequency_id,
                 csp_antenna_info,
                 substation_id,
                 subarray_id,
                 station_id):
        self.logical_channel_id = logical_channel_id  # 0x0
        self.unix_epoch_time = unix_epoch_time  # all packets are 0x6489c7b2 .. 0x6489c7e6 (6 epoch times)
        self.center_freq = center_freq  # 0x9011, expected 0x1011. Again MSbit set
        self.frequency = frequency  # 0x12a05f20 = 312.500.000 Hz = 312.5 MHz
        self.csp_channel_info = csp_channel_info  # ICD seems wrong here; below 3 fields are not subfields of this one.
        self.beam_id = beam_id  # 0x1
        self.frequency_id = frequency_id  # 0x190 = 400
        self.csp_antenna_info = csp_antenna_info  # 0xb001
        self.substation_id = substation_id  # 0x1
        self.subarray_id = subarray_id  # 0x1
        self.station_id = station_id  # 1..6 repeating

    def __str__(self):
        return (f'Packet - spead_logical_channel_id      {hex(self.logical_channel_id)}\n'
                f'Packet - spead_unix_epoch_time         {hex(self.unix_epoch_time)}\n'
                f'Packet - spead_center_freq             {hex(self.center_freq)}\n'
                f'Packet - spead_frequency               {hex(self.frequency)}\n'
                f'Packet - spead_csp_channel_info        {hex(self.csp_channel_info)}\n'
                f'Packet - spead_beam_id                 {hex(self.beam_id)}\n'
                f'Packet - spead_frequency_id            {hex(self.frequency_id)}\n'
                f'Packet - spead_csp_antenna_info        {hex(self.csp_antenna_info)}\n'
                f'Packet - spead_substation_id           {hex(self.substation_id)}\n'
                f'Packet - spead_subarray_id             {hex(self.subarray_id)}\n'
                f'Packet - spead_stationID               {hex(self.station_id)}\n'
                )
