from packet_data_obj.spead_header import SpeadHeader


class PacketData:
    def __init__(self, spead_header: SpeadHeader = None):
        self.__spead_header = spead_header

    @property
    def eth_hdr(self):
        return self.__eth_hdr

    @eth_hdr.setter
    def eth_hdr(self, value):
        self.__eth_hdr = value

    @property
    def ip_header(self):
        return self.__ip_header

    @ip_header.setter
    def ip_header(self, value):
        self.__ip_header = value

    @property
    def udp_header(self):
        return self.__udp_header

    @udp_header.setter
    def udp_header(self, value):
        self.__udp_header = value

    @property
    def spead_header(self):
        return self.__spead_header

    @spead_header.setter
    def spead_header(self, value):
        self.__spead_header = value
