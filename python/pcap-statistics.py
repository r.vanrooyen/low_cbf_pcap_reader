import sys

import numpy as np

import readLFAA


def main(file_name):
    pcap_data = readLFAA.get_pcap_stats(file_name)
    print(f"Data file {pcap_data.file_name}")
    print(f"  has {pcap_data.nr_of_packets} packets of size {pcap_data.packet_size} bytes")
    print(f"  simulating data from {pcap_data.nr_of_stations} stations")
    [_, nchannels] = np.shape(pcap_data.channelfreq_meta_data)
    frequencies = pcap_data.channelfreq_meta_data[0]
    print(f"  for {nchannels} channels with frequencies {frequencies / 1e6} MHz")
    delta_t = pcap_data.unix_epoch_time_delta
    print(f"  approximately {delta_t} seconds representing {delta_t / 0.9:.2f} correlator dumps")


# ################################################################################
# # main on execution of this file
# ################################################################################
if __name__ == '__main__':
    main(sys.argv[1])
