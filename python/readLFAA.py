###############################################################################
#
###############################################################################

"""
Author:
. TOPIC, 26 June 2023
Purpose:
. Display information from captured LFAA packets
. This is a modified version of tc_apertif_unb1_correlator_offload.py
Description:
. Processes PCAP dump files stored by e.g. tcpdump.
Usage:
. Capture a PCAP file:
  . sudo /usr/sbin/tcpdump -vvxSnelfi eth1 -n dst port 4000 -c 1000 -w ~/my_dump_file.pcap
. Examine a PCAP file with this script:
  . python pcap_reader_LFAA.py ~/my_dump_file.pcap
Info:
 . Used ICD: SKA1 LFAA TO CSP INTERFACE CONTROL DOCUMENT 100-000000-004
"""
import struct

import numpy as np
# Do not remove
from scapy.all import rdpcap
from scapy.utils import PcapReader

from common import CommonBytes
from packet_data_obj.packet_data import PacketData
from packet_data_obj.pcap_file_data import PcapFileData
from packet_data_obj.spead_header import SpeadHeader

NOF_POLS = 2
NOF_COMPLEX = 2
NOF_SAMPLES_PER_POL = 2048


def calculate_number_of_stations_and_channels(packet):
    p_data = packet.payload.payload.payload.raw_packet_cache[0:72]
    spead_hdr_struct = struct.unpack('>72B', p_data)
    spead_hdr_bytes = CommonBytes(0, 72)
    for byte_index, byte in enumerate(reversed(spead_hdr_struct)):
        spead_hdr_bytes[byte_index] = byte

    # starts with 1
    nof_stations = spead_hdr_bytes[11:10]
    # starts with 0
    nof_channels = spead_hdr_bytes[61:60]
    return nof_stations, nof_channels + 1


def calculate_number_of_packets(dumpfile):
    number_of_packets = 0
    # Open the pcap file using PcapReader
    with PcapReader(dumpfile) as pcap_packets:
        for packet in pcap_packets:
            number_of_packets += 1
            last_packet = packet

    import os
    file_name = os.path.basename(dumpfile)
    return number_of_packets, last_packet, len(last_packet), file_name


def get_pcap_stats(dumpfile):
    """
    Collect test statistics from file
    """
    number_of_packets, last_packet, packet_size, file_name = calculate_number_of_packets(dumpfile)
    # Calculated base on a .pcap file
    nof_packets = number_of_packets
    # calculate nr of stations and channels
    nof_stations, nof_channels = calculate_number_of_stations_and_channels(last_packet)
    channel_freq_meta_data = np.zeros([nof_stations, nof_channels])
    pcap_file_data = PcapFileData(nr_of_packets=nof_packets, file_name=file_name,
                                  packet_size=packet_size, nr_of_channels=nof_channels,
                                  nr_of_stations=nof_stations)

    with PcapReader(dumpfile) as packets:
        for packet_number, packet in enumerate(packets):
            packet_data = PacketData()
            spead_hdr_raw = packet.payload.payload.payload.raw_packet_cache[0:72]  # 72
            ###########################################################################
            # SPEAD header: 72 big endian bytes
            ###########################################################################
            spead_hdr_struct = struct.unpack(">72B", spead_hdr_raw)
            spead_hdr_bytes = CommonBytes(0, 72)
            for byte_index, byte in enumerate(reversed(spead_hdr_struct)):
                spead_hdr_bytes[byte_index] = byte

            spead_hdr = SpeadHeader(logical_channel_id=spead_hdr_bytes[61:60],
                                    unix_epoch_time=spead_hdr_bytes[45:40],
                                    center_freq=spead_hdr_bytes[31:30],
                                    frequency=spead_hdr_bytes[29:24],
                                    csp_channel_info=spead_hdr_bytes[23:22],
                                    beam_id=spead_hdr_bytes[19:18],
                                    frequency_id=spead_hdr_bytes[17:16],
                                    csp_antenna_info=spead_hdr_bytes[15:14],
                                    substation_id=spead_hdr_bytes[13],
                                    subarray_id=spead_hdr_bytes[12],
                                    station_id=spead_hdr_bytes[11:10]
                                    )
            packet_data.spead_header = spead_hdr
            spead_logical_channel_id = spead_hdr.logical_channel_id
            spead_frequency = spead_hdr.frequency
            spead_station_id = spead_hdr.station_id
            channel_freq_meta_data[spead_station_id - 1, spead_logical_channel_id] = spead_frequency
            pcap_file_data.add_packet(packet_data)
    pcap_file_data.unix_epoch_time_delta = (pcap_file_data.packets[-1].spead_header.unix_epoch_time -
                                            pcap_file_data.packets[0].spead_header.unix_epoch_time)
    pcap_file_data.set_channelfreq_meta_data(channel_freq_meta_data)
    return pcap_file_data
