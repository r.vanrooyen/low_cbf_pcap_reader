###############################################################################
#
# Copyright (C) 2015
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

"""
Author:
. Daniel van der Schuur
Purpose:
. Display information from captured visibility channel packets
Description:
. Processes PCAP dump files stored by e.g. tcpdump.
Usage:
. Capture a PCAP file: 
  . sudo /usr/sbin/tcpdump -vvxSnelfi eth1 -n dst port 4000 -c 1000 -w ~/apertif_unb1_correlator_offload.dump
. Examine a PCAP file with this script:
  . python tc_apertif_unb1_correlator_offload.py ~/apertif_unb1_correlator_offload.dump
  . Use Grep to filter out printed lines/header fiels of interest e.g. '| grep timestamp'
Extra:
. Print back a dumped PCAP file to the screen (example: 1 packet, -c 1):
  . sudo /usr/sbin/tcpdump -e -c 1 -vvX -r ~/apertif_unb1_correlator_offload.dump 
"""

import itertools
import struct
import sys

from common import CommonShorts, CommonBits, CommonBytes, split_list

PRINT_VISIBILITIES = False  # True = print the visibility payload too. Otherwise only the packet header fields are printed.

NOF_INPUTS = 24  # 12 dishes * 2 pols
NOF_VISIBILITIES = (NOF_INPUTS * (NOF_INPUTS + 1)) / 2  # 300
VISIBILITIES = [i for i in itertools.combinations_with_replacement(range(NOF_INPUTS), 2)]


def dumpfile_to_array(dumpfile):
    """
    Returns a complex Numpy Array of dimensions [NOF_CHANNELS][NOF_VISIBILITIES]
    """
    # Our 2d list to return
    channel_visibilities = []

    # Strip off the PCAP header
    with open(dumpfile, "rb") as f:
        pcap_global_hdr_raw = f.read(24)

        # Examine packets but don't append to return list until after channel 0 is found
        append_channels = False

        for packet in range(1000):  # 960 channels = 15 integration periods of 64 channels.
            pcap_pkt_hdr_raw = f.read(16)
            eth_hdr_raw = f.read(14)
            ip_hdr_raw = f.read(20)
            udp_hdr_raw = f.read(8)
            id_hdr_raw = f.read(16)
            flag_hdr_raw = f.read(24)

            visibilities_raw = f.read(2400)

            ###########################################################################
            # Ethernet header: 7 big endian unsigned shorts (16b)
            ###########################################################################
            eth_hdr_struct = struct.unpack('>7H', eth_hdr_raw)
            eth_hdr_shorts = CommonShorts(0, 7)
            for short_index, short in enumerate(reversed(eth_hdr_struct)):
                eth_hdr_shorts[short_index] = short
            print(f'Packet {packet} - ETH Destination MAC          {hex(eth_hdr_shorts[6:4])}')
            print(f'Packet {packet} - ETH Source MAC               {hex(eth_hdr_shorts[3:1])}')
            print(f'Packet {packet} - ETH Ether type               {hex(eth_hdr_shorts[0])}')

            ###########################################################################
            # IP header: 10 big endian shorts (16b) = 160 bits
            ###########################################################################
            ip_hdr_struct = struct.unpack('>10H', ip_hdr_raw)
            ip_hdr_shorts = CommonShorts(0, 10)
            for short_index, short in enumerate(reversed(ip_hdr_struct)):
                ip_hdr_shorts[short_index] = short

            # Convert this to CommonBits so we can use bit indexing
            ip_hdr_bits = CommonBits(ip_hdr_shorts.data, 160)
            print(f'Packet {packet} - IP version                   {hex(ip_hdr_bits[159:156])}')
            print(f'Packet {packet} - IP header length             {ip_hdr_bits[155:152]}')
            print(f'Packet {packet} - IP services                  {hex(ip_hdr_bits[151:144])}')
            print(f'Packet {packet} - IP total length              {ip_hdr_bits[143:128]}')
            print(f'Packet {packet} - IP identification            {hex(ip_hdr_bits[127:112])}')
            print(f'Packet {packet} - IP flags                     {hex(ip_hdr_bits[111:109])}')
            print(f'Packet {packet} - IP fragment offset           {hex(ip_hdr_bits[108:96])}')
            print(f'Packet {packet} - IP time to live              {hex(ip_hdr_bits[95:88])}')
            print(f'Packet {packet} - IP Protocol                  {hex(ip_hdr_bits[87:80])}')
            print(f'Packet {packet} - IP header checksum           {ip_hdr_bits[79:64]}')
            print(f'Packet {packet} - IP source address            {hex(ip_hdr_bits[63:32])}')
            print(f'Packet {packet} - IP destination address       {hex(ip_hdr_bits[31:0])}')

            ###########################################################################
            # UDP header: 4 big endian shorts (16b)
            ###########################################################################
            udp_hdr_struct = struct.unpack('>4H', udp_hdr_raw)
            udp_hdr_shorts = CommonShorts(0, 4)
            for short_index, short in enumerate(reversed(udp_hdr_struct)):
                udp_hdr_shorts[short_index] = short
            print(f'Packet {packet} - UDP destination port        {udp_hdr_shorts[3]}')
            print(f'Packet {packet} - UDP source port             {udp_hdr_shorts[2]}')
            print(f'Packet {packet} - UDP total length            {udp_hdr_shorts[1]}')
            print(f'Packet {packet} - UDP checksum                {udp_hdr_shorts[0]}')

            ###########################################################################
            # ID header: 16 big endian bytes = 128 bits
            ###########################################################################
            id_hdr_struct = struct.unpack('>16B', id_hdr_raw)
            id_hdr_bytes = CommonBytes(0, 16)
            for byte_index, byte in enumerate(reversed(id_hdr_struct)):
                id_hdr_bytes[byte_index] = byte

            # Convert this to CommonBits so we can use bit indexing
            id_hdr_bits = CommonBits(id_hdr_bytes.data, 128)
            print(f'Packet {packet} - id_marker_byte              {id_hdr_bits[127:120]}')
            print(f'Packet {packet} - id_format_version           {id_hdr_bits[119:112]}')
            print(f'Packet {packet} - id_beamlet_index            {id_hdr_bits[111:96]}')
            print(f'Packet {packet} - id_channel_index            {id_hdr_bits[95:80]}')
            print(f'Packet {packet} - id_reserved                 {id_hdr_bits[79:64]}')
            print(f'Packet {packet} - id_timestamp                {id_hdr_bits[63:0]}')

            channel = id_hdr_bits[95:80]
            timestamp = id_hdr_bits[63:0]

            ###########################################################################
            # Flag header: 24 big endian bytes = 192 bits
            ###########################################################################
            flag_hdr_struct = struct.unpack('>24B', flag_hdr_raw)
            flag_hdr_bytes = CommonBytes(0, 24)
            for byte_index, byte in enumerate(reversed(flag_hdr_struct)):
                flag_hdr_bytes[byte_index] = byte

            # Convert this to CommonBits so we can use bit indexing
            flag_hdr_bits = CommonBits(flag_hdr_bytes.data, 192)
            print(f'Packet {packet} - flags_crc_error             {flag_hdr_bits[191:168]}')
            print(f'Packet {packet} - flags_no_input_present      {flag_hdr_bits[167:144]}')
            print(f'Packet {packet} - flags_uploading_weights     {flag_hdr_bits[143:120]}')
            print(f'Packet {packet} - flags_noise_source_enabled  {flag_hdr_bits[119:96]}')
            print(f'Packet {packet} - flags_telescope_pointing_off {flag_hdr_bits[95:72]}')
            print(f'Packet {packet} - flags_antenna_broken        {flag_hdr_bits[71:48]}')
            print(f'Packet {packet} - flags_reserved_0            {flag_hdr_bits[47:24]}')
            print(f'Packet {packet} - flags_reserved_1            {flag_hdr_bits[23:0]}')

            ###########################################################################
            # Visibilities: 300 visibilities * 32b complex (64b total) = 600 
            #               little endian signed integers.
            ###########################################################################
            visibilities_struct = struct.unpack('600i', visibilities_raw)

            # Split the 600-word list into a 300-tuple list
            visibilities_tuples = split_list(visibilities_struct, 2)

            # Convert the tuples to complex
            visibilities_complex = [complex(i[0], i[1]) for i in visibilities_tuples]

            # Print the complex values
            if PRINT_VISIBILITIES:
                for index, value in enumerate(visibilities_complex):
                    print(f'Packet {packet} - Visibility {index:03d} {list(VISIBILITIES[index])} {value}')

            # Tag the moment we've found channel 0.
            if not channel:
                append_channels = True

            if append_channels:
                channel_visibilities.append(visibilities_complex)

    # Return our list [NOF_CHANNELS][NOF_VISIBILITIES]
    return channel_visibilities


################################################################################
# main on execution of this file
################################################################################
if __name__ == '__main__':
    # Read the dump file and pour its visibility content in a 2d array
    dump_arr = dumpfile_to_array(sys.argv[1])

#    # Split the list into sublists of 64 channels (one integration period)
#    integration_periods = split_list(dump_arr, 64)
#
#    for integration_period in integration_periods:
#        # The correlator outputs channels in the same order as the FFT, so reorder first:
#        reordered_integration_period = fft_reorder(integration_period)
#        # Plot the phase shifts
#        plot_phase_shifts(reordered_integration_period, NOF_VISIBILITIES)
